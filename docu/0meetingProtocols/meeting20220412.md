# Meeting 12.04.2022
Start: 16:02  
End:  16:37

Attendence (in Zoom):  
Dusmanu Mihai-Alexandru, Forgaard Theodor, Heine Tom Martin, Kalananthan Senthuran, Steinsland Kristoffer, Weder Silvan 

	1. Situation
		a. Crawler now an interface
			i. Working
			ii. Implemented Search for Coordinates and w3w search
				1) Difficulties using word search or city names
			iii. YouTube search working
			iv. Videos get ranked
			v. Download to folder working
			vi. TODOs: 
				1) Place search
				2) Download method clean up
				3) Installer for Python libraries
		b. Pre-processing
			i. Splitting by frames roughly there
		c. H-Log
			i. Problem Multiple models for one continues video
				1) To many pictures -> split in multiple maps
				2) Sometimes losing track
			ii. Solution
				1) Try more pairs for more links
				2) Extending number of matches and about 10 second timeframes
			iii. Is sequential maps by Colmap utilised in H-Log?
				1) Unsure no, check by running Colmap on the video
				2) Refence to potential solution:
					a) Link: https://github.com/cvg/Hierarchical-Localization/pull/175
			iv. Camera model in H-Log is really basic
				1) Could be changed later on
			v. Framerate
				1) 1-5 FPS is good for now
				2) Make it adaptable
			vi. Colmap map merger useful?
				1) To find out
				2) Look in later on
				3) Loop closure more the Idea to use
	2. Questions:
		a. Interface
			i. Command line fine
			ii. Would be great to have a command to run all of it
		b. 3D Mash tool for presentation in js
			i. Link: https://threejs.org/
		c. Is there a Budget?
			i. Not really, let's keep it as is.
	3. Mid Term Plan
		a. Finished Crawler
		b. Basic pre-processing
		c. Running H-Log on the highest ranked video continuously with one Maps output
	4. Next Meeting
		a. Discussed via Teams or E-Mail
