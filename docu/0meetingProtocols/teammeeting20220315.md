# Meeting 15.03.2022
Start: 17:35  
End:  18:06

Attendence: 
Forgaard Theodor, Heine Tom Martin, Kalananthan Senthuran, Steinsland Kristoffer

	1. Big steps
		a. Data crawling
		b. Storage base
		c. Pre-processing
			i. Transition Cuts
			ii. Face recognition cuts
		d. SfM - partial reconstruction
		e. Merge Map
		f. Output - Present final
	2. Start with 
		a. Ground Data
			i. Taget System: Running on Euler Server!
			ii. Git-Lab as Solution
		b. Data Crawler
			i. In: Put in City Name
			ii. Out: Download files to folder
		c. (pre processing)
			i. In: Folder video Files
			ii. Tasks in order
				1) Generating Image sequences
				2) Split on Transitions
				3) Face recognition
			iii. Out: Folder of Folders for image sequences 
		d. Partial Reconstruction- H-Log 
			i. In: Folder of Folders with image sequences
			ii. Task in Order
				1) Run it!
				2) Test around
				3) Run it with sequences
			iii. Out: Partial 3D Point cloud (semi-dense?)
	3. Assigning responsibilities
		a. Crawl and Pre-process
			i. Tom and Senthruan
		b. Partial Reconstruct and H-Log
			i. Kristoffer and Theodor

		
	
	
