# Meeting 04.05.2022
Start: 16:15  
End:  17:55

Attendence: 
Forgaard Theodor, Heine Tom Martin, Kalananthan Senthuran, Steinsland Kristoffer

	1. w3w update:
		a. place search in progress possibility working next week
	2. code restructure 
		a. KS did a lot of stuff
		b. TH split and reorganized the interface and preprocessing pipeline
		c. changes integrable, decided to stick with the python style KS implemented
		(d.) side mote go away from pickle use JSON to transfer data
	3. problems with euler cluster permissions
		a. each file created read write permission only for the creating person
		b. solving by adding group permissions
	4. running demo pipeline for everyone
		a. getting euler cluster to run
		b. install/clone your one version of city slam in home directory of the euler cluster
		c. be sure to use python version 3.7.12
		d. running the pipeline
	5. open tasks
		a. running the pipeline (SN/\TH)
		b. test running videos
		c. mail to supervisors (next week Tuesday 16 o'clock)(TH)
		d. optimism frame splitting by optical flow (reducing data)
		e. extracting frames for videos and putting in shared folder (ALL)
		f. running pipeline on image sets (ALL)
		g. Video List as a Google.Table to check what is done before you do something (TH)
		h. Multicore videosplittin (SN)

	
	
